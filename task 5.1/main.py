# Решение уравнения методом Гаусса-Жордана

s_Equat = [[2, 1, 1], [6, 2, 1]]  # Система уравнений
s_Len = len(s_Equat)  # Длина системы
s_result = [0, 0]  # Результат решения для двойной системы

# Преобразование методом Гаусса-Джордана
for x in range(s_Len):
    if int(s_Equat[x][x]) == 0:
        break
    for y in range(x + 1, s_Len):
        if x!=y:
            fraction = s_Equat[y][x] / s_Equat[x][x] # Соотносим значения
            for z in range(s_Len + 1):
                s_Equat[y][z] = s_Equat[y][z] - fraction * s_Equat[x][z]
        else:
            break

s_result[s_Len - 1] = s_Equat[s_Len - 1][s_Len] / s_Equat[s_Len - 1][s_Len - 1]  # Первое число

# Нахождение остальных чисел
for x in range(s_Len - 2, -1, -1):
    s_result[x] = s_Equat[x][s_Len]

    for j in range(x + 1, s_Len):
        s_result[x] = s_result[x] - s_Equat[x][y] * s_result[y]

    s_result[x] = s_result[x] / s_Equat[x][x]  # Второе число

print(s_result)