# Вывод анимации

import time
m = []  # Массив для хранения кадров

file = open(r"earth.md")  # Адрес исходного файла
sym = ''.join([line for line in file])  # Получение  символов
numberframe = 1  # Номер кадра

while numberframe < 26:
    frame = sym.split(f'\n```\n')  # Разделение кадров
    m.append(str(frame[numberframe]))  # Добавление кадров в массив
    numberframe += 1
print('\n')

# Вывод анимации (5 раз)
for x in range(1, 5):
    for y in range(0, len(m)):
        if y % 2 == 0:
            print(f'\033[32m{m[y]}\033[0m')  # Вывод кадров
            time.sleep(0.8) # Время смены кадров анимации
            print('\n')
file.close() # Выход из файла
