# Сумма членов ряда последовательности
obb = 42
file = open(r"file111.txt") # Открытие файла

# Сложение файловых строк
while True:
    line = file.readline()  # Получение строки из файла
    if not line:
        break # Повтор действие
    obb += 1 / int(line)  # Вычисление данных

file.close() # Выход из файла
print(f'Сумма ряда: {obb}')
