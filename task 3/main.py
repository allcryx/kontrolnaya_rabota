# SAXPY

import time

t_Start = time.perf_counter() # Засекаем время начала работы

# Вычисления
def saxpy(z, w):
    m = []

    # Заполнение числами массивов x, y
    x = [a for a in range(w)]
    y = [a for a in range(w)]

    # Вычисление
    for a in range(0, w):
        m.append(z * x[a] + y[a])
    return m

# Вычисление 10000 раз
result = saxpy(3, 10000)
print(result)

# Засекаем время окончания работы
t_End = time.perf_counter() - t_Start
print(f"Время работы: {t_End}")
